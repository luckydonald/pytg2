# **PyTG2** #
Python 3 (and 2.7)

# Changed Project URL: [https://github.com/luckydonald/pytg](https://github.com/luckydonald/pytg)

To update your local clone, type 
``` sh
git remote set-url origin https://github.com/luckydonald/pytg.git
```
All updates will happen there.    
This is the last version of pytg2 (0.3.1) which required telejson, fork of vysheng's telegram-cli wich sends you the messages as json.    
Since the offical cli does the same now, pytg2 0.4.0 updated to work with that instead.    
[https://github.com/luckydonald/pytg](https://github.com/luckydonald/pytg)

--------

Old Description:

### Description ###
A Python package that communicates with (a patched version of) the Telegram messenger CLI.
(The [original version](https://github.com/vysheng/tg) did not support some required features.)